locals {
  region = "eu-west-1"

  app_name = "greeting-app"

  env_name = "prod"

  cidr_block = "10.3.0.0/16"

  availability_zones = ["eu-west-1a", "eu-west-1b"]

  public_subnets_cidr = ["10.3.0.0/19", "10.3.64.0/19"]

  private_subnets_cidr = ["10.3.128.0/19", "10.3.192.0/19"]



  tags = {
    project    = "containerised-app-prod"
    created_by = "Terraform"
  }
}
