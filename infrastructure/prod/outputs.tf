
output "ecr_repository_url" {
  value = module.greeting_app.ecr_repository_url
}

output "load_balancer_dns" {
  value = module.greeting_app.load_balancer_dns
}