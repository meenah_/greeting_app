# Greeting_app

## Introduction
------------

The contents of this directory is used to provision the infrastructure
required to host the microservice app in AWS. The infrastructure is
configured using the `greeting_app` Terraform module and can deployed
with a `make` command locally or Gitlab CICD.

This repository contains a microservice app, `Greeting app`, hosted on AWS using Terraform and deployed using Gitlab CICD.

## Deployment Instructions, locally
-----------------------

The terraform aspect can be deployed locally using a [Makefile] making it a "1-click" deployment.
- `~/greeting-app/infrastucture/<env>`
- `make deploy`

The `make deploy` command above runs the following steps:

-   Creates the AWS ECR repo which will store our microservice Docker image

-   Deploys the infrastructure which serves the microservice

Once deployed successfully, the load balancer and ECR url will be outputted in the terminal.

The docker image is built and pushed using Gitlab. [README for deploying using Gitlab CICD](../README.md)