# --- modules/services/ecs-frontend ---
resource "aws_iam_role_policy" "ecs_execution_iam_role_policy" {
  name   = "${var.app_name}-${var.env_name}"
  policy = data.aws_iam_policy_document.ecs_execution_policy.json
  role   = aws_iam_role.ecs_execution_role.id
}
