
output "ecr_repository_url" {
  value = aws_ecr_repository.greeting_app.repository_url
}

output "load_balancer_dns" {
  value = aws_lb.greeting_app.dns_name
}