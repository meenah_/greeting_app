resource "aws_ecr_repository" "greeting_app" {
  name         = "${var.app_name}-${var.env_name}"
  force_delete = true
}