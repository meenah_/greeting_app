#Cluster
resource "aws_ecs_cluster" "greeting_app" {
  name = "${var.app_name}-${var.env_name}"

}

#Task-definition
resource "aws_ecs_task_definition" "greeting_app" {
  family                   = "${var.app_name}-${var.env_name}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
  cpu                      = 1024
  memory                   = 2048
  container_definitions = jsonencode([ #converts hcl to json
    {
      name      = "${var.app_name}-${var.env_name}"
      image     = data.terraform_remote_state.global.outputs.ecr_repository_url
      cpu       = 10
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 5000
          hostPort      = 5000
        }
      ]
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = "ecs/${var.app_name}-${var.env_name}"
          awslogs-region        = var.region
          awslogs-stream-prefix = "ecs"
        }
      },
    }
  ])
  tags = var.tags
}

#Service
resource "aws_ecs_service" "greeting_app" {
  name            = "${var.app_name}-${var.env_name}"
  cluster         = aws_ecs_cluster.greeting_app.id
  task_definition = aws_ecs_task_definition.greeting_app.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.greeting_app_ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.greeting_app.arn
    container_name   = "${var.app_name}-${var.env_name}"
    container_port   = 5000
  }

  depends_on = [aws_lb_listener.https_forward]

  tags = var.tags
}