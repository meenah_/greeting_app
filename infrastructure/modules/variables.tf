variable "cidr_block" {
  type = string
}

variable "public_subnets_cidr" {
  type        = list(any)
  description = "Public Subnet CIDR values"
}

variable "private_subnets_cidr" {
  type        = list(any)
  description = "Private Subnet CIDR values"
}

variable "availability_zones" {
  type = list(any)
}

variable "app_name" {
  description = "The name of the containerised app"
  type        = string
}

variable "region" {
  type = string
}

variable "env_name" {
  type = string

}

variable "tags" {
  type = any

}