locals {
  region = "eu-west-1"

  app_name = "greeting-app"

  env_name = "dev"

  cidr_block = "10.0.0.0/16"

  availability_zones = ["eu-west-1a", "eu-west-1b"]

  public_subnets_cidr = ["10.0.0.0/18", "10.0.64.0/18"]

  private_subnets_cidr = ["10.0.128.0/18", "10.0.192.0/18"]



  tags = {
    project    = "containerised-app-dev"
    created_by = "Terraform"
  }
}

