# This app greets the user according to the time.
from flask import Flask, render_template, request
from datetime import datetime


app = Flask(__name__)

@app.route("/")
def name_form():
    return render_template('name_form.html')

@app.route("/greeting/", methods = ['POST', 'GET'])
def greet():
    if request.method == 'GET':
        return f"Try going to '/' to submit your name"
    if request.method == 'POST':
        greeting = get_greeting()
        name = request.form
        return render_template('greeting_data.html', greeting = greeting, name = name["Name"])

def get_greeting():
    current_time = datetime.now().hour

    if current_time < 12:
        return("Morning")
    elif current_time < 18:
        return("Afternoon")
    else:
        return("Evening")

if __name__ == "__main__":
    app.run(threaded=True, host='0.0.0.0', port=5000)
    