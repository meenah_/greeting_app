# Greeting App

## Introduction
------------

This directory contains a simple Flask app which takes an input and
returns greeting based on the time of day.

### Prerequisites, local

Ensure you have any prerequisites installed first by executing the
following:

    # If you use asdf
    asdf plugin add python
    asdf install

    # If you use Brew
    brew install python

    # For all tools
    pip3 install pipenv
    pipenv install

### Running the app

The following command can then be executed if you wish to try out the
app locally:

    flask --app greeting_app run

You will then see an output similar to the following:

    * Serving Flask app 'greeting_app'
     * Debug mode: off
    WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
     * Running on http://127.0.0.1:5000
    Press CTRL+C to quit

The provided URL can be used to access the app.