FROM python:3.10-alpine

WORKDIR /greeting-app

COPY requirements.txt app/ /greeting-app/
RUN pip3 --no-cache-dir install -r requirements.txt

CMD gunicorn --bind 0.0.0.0:5000 greeting_app:app