# Greeting_app

## Introduction
------------

This project provides an example of a [containerised Python application](../app/README.md)
hosted on AWS. 

At a high level the following resources are provisioned:

-   Elastic Container Registry (ECR) - for storage of the Docker image
-   Application Load Balancer (ALB) - to distribute incoming traffic to
    Fargate (the serverless compute service for Elastic Container
    Service (ECS)) and also provides a url for the app rather than using ephemeral ip addresses.
-   ECS Cluster - for grouping the container resources (services and
    tasks)
-   ECS service - for running the containerised microservice task
-   Cloudwatch - to allow logging of the microservice task
-   S3 is used to store remote state file 
  
`pre-commit` is used to format and validate the terraform files before it pushed to gitlab repo.

## Deployment Instructions
The pipeline has 3 stages,

- terraform: this deploys the infrastructure on AWS to dev and prod environments
- build: this builds the docker image and pushes to ECR
- deploy: this updates the ECS service and pulls the latest image from ECR


Ideally, there should be at least 3 environments: 
- `dev`
- `test`, mirrors the prod environment and catches bugs before it is deployed to prod, but I've decided to use just `dev` and `prod` for this exercise.
- `prod`
  
To deploy the terraform infrastructure locally using `makefile`, see [Infrastructure-Readme](../infrastructure/README.md)

### Further Improvements
- Use different Dockerfile for dev and prod, prod's Dockerfile should only have the bare necessities installed or use multistage Dockerfiles so we can have multiple docker images from the same Dockerfile to prevent having different dependencies when one Dockerfile is updated and one isnt.
    Also, having different Dockerfile in this project will prevent the dev `build-dev` and `deploy-dev` jobs from runing when dev branch is merged to main by using Gitlab `change` to only allow Dockerfile to be built when there's a change in the Dockerfile.
    ```
        .env-rules: 
            rules:
                - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
                  when: never
                - if: $CI_COMMIT_REF_NAME == $CI_COMMIT_BRANCH
                  changes:
                    - Dockerfile

    ```
- Use Route53, a DNS
- Use autoscaling to scale as traffic increases 
- Use VPC endpoint to access images in ECR to keep traffic internal and save cost
  
### Diagrams
- ![Infrastructure Diagram](GreetingApp-ECS.drawio.png)
- ![App Diagram](Screenshot%202023-04-10%20at%2023.15.06.png)
- ![App Diagram](Screenshot%202023-04-10%20at%2023.16.10.png)

